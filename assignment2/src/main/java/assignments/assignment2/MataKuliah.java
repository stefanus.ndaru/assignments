package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int kapasitasSekarang;


    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[this.kapasitas];
        this.kapasitasSekarang = 0;
    }

    public String getNama(){
        return this.nama;
    }

    public String getKode(){
        return this.kode;
    }

    public int getSKS(){
        return this.sks;
    }

    public int getKapasitasSekarang(){
        return this.kapasitasSekarang;
    }

    public int getKapasitas(){
        return this.kapasitas;
    }

    public Mahasiswa[] getMahasiswaList(){
        return this.daftarMahasiswa;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public void setKode(String kode){
        this.kode = kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // finds empty erray in daftarMahasiswa and adds the mahasiswa

        int emptyIndex = this.obtainEmptyArraySpace();
        this.daftarMahasiswa[emptyIndex] = mahasiswa;
        this.kapasitasSekarang++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        // drops mahasiswa from the array
        // finds the mahasiswa in the array, replace it with null
        // shift the rest to the left

        int studentIndex = this.findMahasiswa(mahasiswa);
        this.daftarMahasiswa[studentIndex] = null;
        for(int i = studentIndex; i < this.daftarMahasiswa.length; i++){
            if (i == this.daftarMahasiswa.length-1) {
                this.daftarMahasiswa[i] = null;
                break;
            }
            this.daftarMahasiswa[i] = this.daftarMahasiswa[i+1];
            if(this.daftarMahasiswa[i+1] == null) break;
        }
        this.kapasitasSekarang--;
    }

    public String toString() {
        return this.getNama();
    }

    public boolean isCapacityFull(){
        if(this.kapasitasSekarang == this.kapasitas) return true;
        return false;
    }

    public String printMahasiswaInClass(){
        String output = "";
        if(this.getKapasitasSekarang() == 0) output += "Belum ada mahasiswa yang mengambil mata kuliah ini.";
        else{
            output += ("1. " + this.getMahasiswaList()[0]);
            for(int i = 1; i < this.getMahasiswaList().length; i++){
                if(this.getMahasiswaList()[i] != null){
                    int n = i+1;
                    output += ("\n" + n + ". " + this.getMahasiswaList()[i]);
                }
            }
        }
        return output;
    }

    private int obtainEmptyArraySpace(){
        for(int i = 0; i < this.daftarMahasiswa.length; i++){
            if (this.daftarMahasiswa[i] == null) {
                return i;
            }
        }
        return 999;
    }

    private int findMahasiswa(Mahasiswa mahasiswa) {
        for(int i = 0; i < this.daftarMahasiswa.length; i++){
            if (mahasiswa.getNama().equals(this.daftarMahasiswa[i].getNama())) return i;    
        }
        return 999;
    }

}
