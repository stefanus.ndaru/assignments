package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {
    
    public static long specialDigitGenerator(String inputString) {
        // special digit generator merupakan method yang akan memproses digit terakhir dari NPM
        long specialDigit = 0;

        // menggunakan palindrome algorithm untuk memperoleh pola digit terakhir
        for(int i = 0; i < inputString.length()/2; i++) {
            specialDigit += Character.getNumericValue(inputString.charAt(i))*Character.getNumericValue(inputString.charAt(inputString.length()-1-i));
        }
        specialDigit += Character.getNumericValue(inputString.charAt(inputString.length()/2));

        // memproses apabila digit yang dibentuk terdiri dari 2 digit atau lebih
        while (specialDigit > 9) {
            long temp = specialDigit;
            specialDigit = 0;
            while (temp > 0) {
                specialDigit += temp % 10;
                temp /= 10;
            }
        }
        return specialDigit;
    }

    public static boolean validate(long npm) {
        // Memvalidasi NPM dan akan mereturn boolean benar atau tidak

        if (Long.toString(npm).length() == 14 ) {
            String rawInputString = Long.toString(npm);
            String inputString = rawInputString.substring(0, rawInputString.length()-1);

            // menginitialize setiap digit yang akan divalidasi
            long firstTwoDigit = Long.parseLong(inputString.substring(0,2));
            long yearOfBirth = Long.parseLong(inputString.substring(10,12));
            long lastDigitNpmCode = Long.parseLong(rawInputString.substring(rawInputString.length()-1, rawInputString.length()));
            long age = firstTwoDigit-yearOfBirth;

            // memanggil method special digit untuk memperoleh digit terakhir
            long specialDigit = specialDigitGenerator(inputString);

            if(
                (
                (inputString.substring(2,4)).equals("01") ||
                (inputString.substring(2,4)).equals("02") ||
                (inputString.substring(2,4)).equals("03") ||
                (inputString.substring(2,4)).equals("11") ||
                (inputString.substring(2,4)).equals("12")
                ) && age >= 15 && specialDigit == lastDigitNpmCode) {
                    return true;
            }
        }
        return false;
    }

    public static String extract(long npm) {
        // Method untuk mengekstrak informasi dari NPM dan akan direturn dengan sebuah string info

        String rawInputString = Long.toString(npm);
        String year = "20" + rawInputString.substring(0,2);
        String jurusan = "";
        String dob = rawInputString.substring(4,6) + "-" + rawInputString.substring(6,8) + "-" +rawInputString.substring(8,12);
        
        switch (Integer.parseInt(rawInputString.substring(2,4))) {
            case 01:
                jurusan += "Ilmu Komputer";
                break;
            case 02:
                jurusan += "Sistem Informasi";
                break;
            case 03:
                jurusan += "Teknologi Informasi";
                break;
            case 11:
                jurusan += "Teknik Telekomunikasi";
                break;
            case 12:
                jurusan += "Teknik Elektro";
                break;
            default:
                break;
        }

        return String.format("Tahun masuk: %1$s\nJurusan: %2$s\nTanggal Lahir: %3$s", year, jurusan, dob);
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            // pemrosesan input 
            
            if (validate(npm)) {
                System.out.println(extract(npm));
            }
            else System.out.println("NPM tidak valid!");
            
        }
        input.close();
    }
}