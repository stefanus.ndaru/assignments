package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{

    private JPanel panelNorth;
    private JPanel panelCenter;
    private JPanel panelSouth;

    public static Color mainBG = new Color(3,9,44);
    public static Color secBG = new Color(12,18,51);
    public static Color btnColorBlue = new Color(73,132,249);
    public static Color btnColorRed = new Color(230,53,101);
    public static Color greyFade = new Color(155,158,169);

    Font gainFont = new Font("Tahoma", Font.PLAIN, 12);  
    Font lostFont = new Font("Tahoma", Font.ITALIC, 12);

    private String hintNama = "Masukkan Nama Lengkap";
    private String hintNPM = "Masukkan NPM";

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
       
        // initiate labels
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.WHITE);

        JLabel footerLabel = new JLabel();
        footerLabel.setText("Created by: Stefanus Ndaru - 2006526812");
        footerLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        footerLabel.setHorizontalAlignment(JLabel.CENTER);
        footerLabel.setForeground(greyFade);
    
        JLabel nama = new JLabel("Nama: ");
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        nama.setForeground(greyFade);

        JLabel npm = new JLabel("NPM: ");
        npm.setAlignmentX(Component.CENTER_ALIGNMENT);
        npm.setForeground(greyFade);

        // initiate separator
        JSeparator sepr1 = new JSeparator();
        sepr1.setMaximumSize(new Dimension(320, 5));
        JSeparator sepr2 = new JSeparator();
        sepr2.setMaximumSize(new Dimension(320, 5));

        // initiate textfields
        JTextField namaField = new HintTextField(hintNama, sepr1, nama);

        JTextField npmField = new HintTextField(hintNPM, sepr2, npm);

        // initiate buttons
        JButton add = new CustomButton("Tambahkan", Color.WHITE, btnColorBlue, btnColorBlue, Color.WHITE, false);
        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if((npmField.getText().equals(hintNPM) || 
                    namaField.getText().equals(hintNama) ||
                    npmField.getText().equals("") ||
                    namaField.getText().equals(""))){
                        JOptionPane.showMessageDialog(null, "Mohon isi seluruh Field");
                }
                else{
                    try{
                        long npm = Long.parseLong(npmField.getText());
                        String nama = namaField.getText();
                        addMahasiswa(nama, npm, frame, daftarMahasiswa, daftarMataKuliah);
                    }
                    catch (NumberFormatException er){
                        JOptionPane.showMessageDialog(null, "Terdapat kesalahan input");
                    }
                }

                // resets the field
                namaField.setText(hintNama);
                namaField.setFont(lostFont);
                namaField.setForeground(greyFade);
                sepr1.setBackground(Color.WHITE);

                npmField.setText(hintNPM);
                npmField.setFont(lostFont);
                npmField.setForeground(greyFade);
                sepr2.setBackground(Color.WHITE);
            }
        });

        JButton back = new CustomButton("Kembali", btnColorRed, btnColorRed, Color.WHITE, btnColorRed, true);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });

        // initiate panels
        panelNorth = new JPanel();
        panelCenter = new JPanel();
        panelSouth = new JPanel();

        frame.setLayout(new BorderLayout());

        // north panel layout
        panelNorth.setBorder(BorderFactory.createEmptyBorder(20,5,20,5));
        panelNorth.setBackground(mainBG);

        // center panel layout
        panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.PAGE_AXIS));
        panelCenter.setBorder(BorderFactory.createEmptyBorder(20,5,5,5));
        panelCenter.setBackground(secBG);

        // south panel layout
        panelSouth.setBackground(mainBG);
        panelSouth.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

        // north panel content
        panelNorth.add(titleLabel);

        // center panel content
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(nama);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(namaField);
        panelCenter.add(Box.createRigidArea(new Dimension(5,2)));
        panelCenter.add(sepr1);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(npm);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(npmField);
        panelCenter.add(Box.createRigidArea(new Dimension(5,2)));
        panelCenter.add(sepr2);
        panelCenter.add(Box.createRigidArea(new Dimension(5,40)));
        panelCenter.add(add);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(back);

        // south panel content
        panelSouth.add(footerLabel);

        // add panel to frame
        frame.add(panelNorth, BorderLayout.NORTH);
        frame.add(panelCenter, BorderLayout.CENTER);
        frame.add(panelSouth, BorderLayout.SOUTH);

    }

    void addMahasiswa(String nama, long npm, JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // adds mahasiswa to the arraylist
        
        boolean found = false;
        for (Mahasiswa mhs : daftarMahasiswa){
            if(mhs.getNpm() == npm){
                JOptionPane.showMessageDialog(null, "NPM " + npm + " sudah pernah ditambahkan sebelumnya");
                found = true;
                break;
            }
        }

        if(!found){
            daftarMahasiswa.add(new Mahasiswa(nama, npm));
            JOptionPane.showMessageDialog(null, "Mahasiswa " + npm + " - " + nama + " berhasil ditambahkan");
            
            frame.getContentPane().removeAll();
            frame.repaint();

            new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            frame.setVisible(true);
        }
    }
    
}
