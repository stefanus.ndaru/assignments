package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{

    private JPanel panelNorth;
    private JPanel panelCenter;
    private JPanel panelSouth;

    public static Color mainBG = new Color(3,9,44);
    public static Color secBG = new Color(12,18,51);
    public static Color btnColorBlue = new Color(73,132,249);
    public static Color greyFade = new Color(155,158,169);
    public static Color btnColorRed = new Color(230,53,101);

    Font gainFont = new Font("Tahoma", Font.PLAIN, 12);  
    Font lostFont = new Font("Tahoma", Font.ITALIC, 12);

    private String hintKode = "Masukkan kode mata kuliah (CS/IK/SI)";
    private String hintNama = "Masukkan nama mata kuliah";
    private String hintSKS = "Masukkan jumlah SKS";
    private String hintKapasitas = "Masukkan kapasitas mata kuliah";

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // initiate labels
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah MataKuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.WHITE);

        JLabel footerLabel = new JLabel();
        footerLabel.setText("Created by: Stefanus Ndaru - 2006526812");
        footerLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        footerLabel.setHorizontalAlignment(JLabel.CENTER);
        footerLabel.setForeground(greyFade);

        JLabel kode = new JLabel("Kode Mata Kuliah: ");
        kode.setAlignmentX(Component.CENTER_ALIGNMENT);
        kode.setForeground(greyFade);

        JLabel nama = new JLabel("Nama Mata Kuliah: ");
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        nama.setForeground(greyFade);
        
        JLabel sks = new JLabel("SKS: ");
        sks.setAlignmentX(Component.CENTER_ALIGNMENT);
        sks.setForeground(greyFade);

        JLabel kapasitas = new JLabel("Kapasitas: ");
        kapasitas.setAlignmentX(Component.CENTER_ALIGNMENT);
        kapasitas.setForeground(greyFade);

        // initiate separator
        JSeparator sepr1 = new JSeparator();
        sepr1.setMaximumSize(new Dimension(320, 5));
        JSeparator sepr2 = new JSeparator();
        sepr2.setMaximumSize(new Dimension(320, 5));
        JSeparator sepr3 = new JSeparator();
        sepr3.setMaximumSize(new Dimension(320, 5));
        JSeparator sepr4 = new JSeparator();
        sepr4.setMaximumSize(new Dimension(320, 5));

        // initiate textfields
        JTextField kodeField = new HintTextField(hintKode, sepr1, kode);
        
        JTextField namaField = new HintTextField(hintNama, sepr2, nama);
        
        JTextField sksField = new HintTextField(hintSKS, sepr3, sks);
        
        JTextField kapasitasField = new HintTextField(hintKapasitas, sepr4, kapasitas);

        // initiate buttons
        JButton add = new CustomButton("Tambahkan", Color.WHITE, btnColorBlue, btnColorBlue, Color.WHITE, false);
        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if((kodeField.getText().equals(hintKode) || 
                    namaField.getText().equals(hintNama) ||
                    sksField.getText().equals(hintSKS) ||
                    kapasitasField.getText().equals(hintKapasitas) ||
                    kodeField.getText().equals("") ||
                    namaField.getText().equals("") ||
                    sksField.getText().equals("") ||
                    kapasitasField.getText().equals(""))){
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh Field");
                }
                else{
                    try{
                        String kode = kodeField.getText();
                        String nama = namaField.getText();
                        int sks = Integer.parseInt(sksField.getText());
                        int kapasitas = Integer.parseInt(kapasitasField.getText());
                        addMataKuliah(kode, nama, sks, kapasitas, frame, daftarMahasiswa, daftarMataKuliah);
                    }
                    catch (NumberFormatException er){
                        JOptionPane.showMessageDialog(null, "Terdapat kesalahan input");
                    }
                }

                // resets the field
                kodeField.setText(hintKode);
                kodeField.setFont(lostFont);
                kodeField.setForeground(greyFade);
                sepr1.setBackground(Color.WHITE);

                namaField.setText(hintNama);
                namaField.setFont(lostFont);
                namaField.setForeground(greyFade);
                sepr2.setBackground(Color.WHITE);

                sksField.setText(hintSKS);
                sksField.setFont(lostFont);
                sksField.setForeground(greyFade);
                sepr3.setBackground(Color.WHITE);

                kapasitasField.setText(hintKapasitas);
                kapasitasField.setFont(lostFont);
                kapasitasField.setForeground(greyFade);
                sepr4.setBackground(Color.WHITE);
            }
        });

        JButton back = new CustomButton("Kembali", btnColorRed, btnColorRed, Color.WHITE, btnColorRed, true);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });


        // initiate panels
        panelNorth = new JPanel();
        panelCenter = new JPanel();
        panelSouth = new JPanel();

        frame.setLayout(new BorderLayout());

        // north panel layout
        panelNorth.setBorder(BorderFactory.createEmptyBorder(20,5,20,5));
        panelNorth.setBackground(mainBG);

        // center panel layout
        panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.PAGE_AXIS));
        panelCenter.setBorder(BorderFactory.createEmptyBorder(20,5,5,5));
        panelCenter.setBackground(secBG);

        // south panel layout
        panelSouth.setBackground(mainBG);
        panelSouth.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

        // north panel content
        panelNorth.add(titleLabel);

        // center panel content
        panelCenter.add(kode);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(kodeField);
        panelCenter.add(Box.createRigidArea(new Dimension(5,2)));
        panelCenter.add(sepr1);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        
        panelCenter.add(nama);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(namaField);
        panelCenter.add(Box.createRigidArea(new Dimension(5,2)));
        panelCenter.add(sepr2);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        
        panelCenter.add(sks);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(sksField);
        panelCenter.add(Box.createRigidArea(new Dimension(5,2)));
        panelCenter.add(sepr3);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        
        panelCenter.add(kapasitas);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(kapasitasField);
        panelCenter.add(Box.createRigidArea(new Dimension(5,2)));
        panelCenter.add(sepr4);
        panelCenter.add(Box.createRigidArea(new Dimension(5,40)));
        
        panelCenter.add(add);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(back);

        // south panel content
        panelSouth.add(footerLabel);

        // add panel to frame
        frame.add(panelNorth, BorderLayout.NORTH);
        frame.add(panelCenter, BorderLayout.CENTER);
        frame.add(panelSouth, BorderLayout.SOUTH);

    }

    void addMataKuliah(String kode, String nama, int sks, int kapasitas, JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // adds matakuliah to the arraylist
        
        boolean found = false;
        for (MataKuliah mk : daftarMataKuliah){
            if(mk.getNama().equals(nama)){
                JOptionPane.showMessageDialog(null, "Mata Kuliah " + nama + " sudah pernah ditambahkan sebelumnya");
                found = true;
                break;
            }
        }

        if(!found){
            daftarMataKuliah.add(new MataKuliah(kode, nama, sks, kapasitas));
            JOptionPane.showMessageDialog(null, "Mata Kuliah " + nama + " berhasil ditambahkan");
            
            frame.getContentPane().removeAll();
            frame.repaint();

            new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            frame.setVisible(true);
        }
    }
    
}
