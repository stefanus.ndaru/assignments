package assignments.assignment4.frontend;

import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI{

    private JPanel panelNorth;
    private JPanel panelCenter;
    private JPanel panelSouth;

    private JButton tambahMahasiswaBtn;
    private JButton tambahMatkulBtn;
    private JButton tambahIRSBtn;
    private JButton hapusIRSBtn;
    private JButton ringkasanMhsBtn;
    private JButton ringkasanMatkulBtn;
    private JButton close;

    public static Color mainBG = new Color(3,9,44);
    public static Color secBG = new Color(12,18,51);
    public static Color btnColorRed = new Color(230,53,101);
    public static Color btnColorBlue = new Color(73,132,249);
    public static Color btnColorOrange = new Color(237,131,31);
    public static Color btnColorGreen = new Color(56,199,101);
    public static Color greyFade = new Color(155,158,169);
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // initiate labels
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.WHITE);

        JLabel footerLabel = new JLabel();
        footerLabel.setText("Created by: Stefanus Ndaru - 2006526812");
        footerLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        footerLabel.setHorizontalAlignment(JLabel.CENTER);
        footerLabel.setForeground(greyFade);

        // initiate buttons
        tambahMahasiswaBtn = new CustomButton("Tambah Mahasiswa", Color.WHITE, btnColorGreen, btnColorGreen, Color.WHITE, false);
        tambahMahasiswaBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });

        tambahMatkulBtn = new CustomButton("Tambah Mata Kuliah", Color.WHITE, btnColorGreen, btnColorGreen, Color.WHITE, false);
        tambahMatkulBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });

        tambahIRSBtn = new CustomButton("Tambah IRS", Color.WHITE, btnColorBlue, btnColorBlue, Color.WHITE, false);
        tambahIRSBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });

        hapusIRSBtn = new CustomButton("Hapus IRS", Color.WHITE, btnColorBlue, btnColorBlue, Color.WHITE, false);
        hapusIRSBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });

        ringkasanMhsBtn = new CustomButton("Lihat Ringkasan Mahasiswa", Color.WHITE, btnColorOrange, btnColorOrange, Color.WHITE, false);
        ringkasanMhsBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });

        ringkasanMatkulBtn = new CustomButton("Lihat Ringkasan Mata Kuliah", Color.WHITE, btnColorOrange, btnColorOrange, Color.WHITE, false);
        ringkasanMatkulBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });

        close = new CustomButton("Tutup Program", btnColorRed, btnColorRed, Color.WHITE, btnColorRed, true);
        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        // initiate separators
        JSeparator sepr1 = new JSeparator();
        sepr1.setMaximumSize(new Dimension(320, 5));

        // initiate panels
        panelNorth = new JPanel();
        panelCenter = new JPanel();
        panelSouth = new JPanel();

        frame.setLayout(new BorderLayout());

        // north panel layout
        panelNorth.setBorder(BorderFactory.createEmptyBorder(20,5,20,5));
        panelNorth.setBackground(mainBG);

        // center panel layout
        panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.PAGE_AXIS));
        panelCenter.setBorder(BorderFactory.createEmptyBorder(20,5,5,5));
        panelCenter.setBackground(secBG);

        // south panel layout
        panelSouth.setBackground(mainBG);
        panelSouth.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

        // north panel content
        panelNorth.add(titleLabel);

        // center panel content
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(tambahMahasiswaBtn);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(tambahMatkulBtn);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(tambahIRSBtn);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(hapusIRSBtn);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(ringkasanMhsBtn);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(ringkasanMatkulBtn);
        panelCenter.add(Box.createRigidArea(new Dimension(5,150)));

        panelCenter.add(sepr1);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));

        panelCenter.add(close);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));

        // south panel content
        panelSouth.add(footerLabel);

        // add panel to frame
        frame.add(panelNorth, BorderLayout.NORTH);
        frame.add(panelCenter, BorderLayout.CENTER);
        frame.add(panelSouth, BorderLayout.SOUTH);

    }
}
