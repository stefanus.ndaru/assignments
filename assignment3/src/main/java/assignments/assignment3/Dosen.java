package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    private MataKuliah mataKuliah;

    public Dosen(String nama) {
        super("Dosen", nama);
    }

    public MataKuliah getMataKuliah(){
        return this.mataKuliah;
    }

    public boolean validateMengajar(MataKuliah mataKuliah){
        // checks if Dosen is currently teaching a Matkul or not, or if the target Matkul already has a Dosen

        if(this.mataKuliah != null){
            System.out.println(String.format("[DITOLAK] %s sudah mengajar mata kuliah %s", this.getName(), this.getMataKuliah()));
            return false;
        }
        else if(mataKuliah.getDosen() != null){
            System.out.println(String.format("[DITOLAK] %s sudah memiliki dosen pengajar", mataKuliah.getName()));
            return false;
        }
        return true;
    }

    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        // set the Matkul for this Dosen, checks for validity first

        if(validateMengajar(mataKuliah)){
            mataKuliah.addDosen(this);
            this.mataKuliah = mataKuliah;
            System.out.println(String.format("%s mengajar mata kuliah %s", this.getName(), mataKuliah.getName()));
        }
    }

    public void dropMataKuliah() {
        // resets the Matkul for this Dosen, prints error if doesn't teach any
        
        if(this.mataKuliah != null){
            System.out.println(String.format("%s berhenti mengajar %s", this.getName(), mataKuliah.getName()));
            this.mataKuliah.dropDosen();
            this.mataKuliah = null;
        }
        else{
            System.out.println(String.format("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun", this.getName()));
        }
    }
}