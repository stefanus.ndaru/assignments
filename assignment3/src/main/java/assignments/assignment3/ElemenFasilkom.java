package assignments.assignment3;

import java.util.*;

abstract class ElemenFasilkom {

    public static int totalElement = 0;

    private String tipe;
    private String nama;
    private int friendship;
    private int currentGreetingAmount;
    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    public ElemenFasilkom(String tipe, String nama){
        this.nama = nama;
        this.tipe = tipe;
    }

    public String getType(){
        return this.tipe;
    }

    public String getName(){
        return this.nama;
    }

    public int getFriendship(){
        return this.friendship;
    }

    public int getCurrentGreeting(){
        return this.currentGreetingAmount;
    }

    public void setFriendship(int friends){
        // Friendship value ranged from 0-100

        this.friendship = friends;
        this.friendship = (this.friendship > 100) ? 100 : this.friendship;
        this.friendship = (this.friendship < 1) ? 0 : this.friendship;
    }

    public int findEmptyArrayMenyapa(){
        for(int i = 0; i < this.telahMenyapa.length; i++){
            if(telahMenyapa[i] == null)
                return i;
        }
        return 999;
    }

    public ElemenFasilkom checkMenyapa(ElemenFasilkom elemenFasilkom){
        // checks if element is already in the array telahMenyapa
        
        for(ElemenFasilkom element : telahMenyapa){
            if(element == null) return null;
            if((element.getName()).equals(elemenFasilkom.getName()))
                return element;
        }
        return null;
    }

    public boolean checkRelationDosenMahasiswa(ElemenFasilkom elemenFasilkom){
        // checks if both elements are Mahasiswa and Dosen, and then checks if they correlate with each other in Matkul

        if(elemenFasilkom instanceof Dosen && this instanceof Mahasiswa){
            Mahasiswa thisMahasiswa = (Mahasiswa) this;
            Dosen thisDosen = (Dosen) elemenFasilkom;
            if(thisDosen.getMataKuliah() != null && thisMahasiswa.checkMataKuliah(thisDosen.getMataKuliah()) != null)
                return true;
        }
        else if(this instanceof Dosen && elemenFasilkom instanceof Mahasiswa){
            Mahasiswa thisMahasiswa = (Mahasiswa) elemenFasilkom;
            Dosen thisDosen = (Dosen) this;
            if(thisDosen.getMataKuliah() != null && thisMahasiswa.checkMataKuliah(thisDosen.getMataKuliah()) != null)
                return true;
        }
        return false;
    }

    public void menyapa(ElemenFasilkom elemenFasilkom) {
        // menyapa method, first check if they have menyapa before or not

        if(this.checkMenyapa(elemenFasilkom) != null)
            System.out.println(String.format("[DITOLAK] %s telah menyapa %s hari ini", this.getName(), elemenFasilkom.getName()));
        else{
            // and then checks if they are Dosen and Mahasiswa

            if(this.checkRelationDosenMahasiswa(elemenFasilkom)){
                this.setFriendship(this.getFriendship() + 2);
                elemenFasilkom.setFriendship(elemenFasilkom.getFriendship() + 2);
            }

            // finally adds both participant to their corresponding telahMenyapa array
            // currentGreeting amount is used to determine the score for Next Day

            this.currentGreetingAmount += 1;
            elemenFasilkom.currentGreetingAmount += 1;

            int emptyIndex = this.findEmptyArrayMenyapa();
            this.telahMenyapa[emptyIndex] = elemenFasilkom;
            
            int otherEmptyIndex = elemenFasilkom.findEmptyArrayMenyapa();
            elemenFasilkom.telahMenyapa[otherEmptyIndex] = this;

            System.out.println(String.format("%s menyapa dengan %s", this.getName(), elemenFasilkom.getName()));
        }
    }

    public void resetMenyapa() {
        for(int i = 0; i < this.telahMenyapa.length; i++){
            if(telahMenyapa[i] == null) break;
            telahMenyapa[i] = null;
        }
        this.currentGreetingAmount = 0;
    }

    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        // membeli makanan method, the elements will be guaranteed because it is handled in Main
        // checks if ElemenKantin is selling the specified food

        ElemenKantin thisElemenKantin = (ElemenKantin) penjual;
        if(thisElemenKantin.findSpecificFood(namaMakanan) != null){
            // if sells, adds friendship and buys food
            Makanan food = thisElemenKantin.findSpecificFood(namaMakanan);

            thisElemenKantin.setFriendship(thisElemenKantin.getFriendship() + 1);
            pembeli.setFriendship(pembeli.getFriendship() + 1);

            System.out.println(String.format("%s berhasil membeli %s seharga %d", pembeli.getName(), food.getName(), food.getHarga()));
        }
        else{
            // if not, prints out error message
            System.out.println(String.format("[DITOLAK] %s tidak menjual %s", penjual.getName(), namaMakanan));
        }
    }

    public String toString() {
        return this.nama;
    }
}