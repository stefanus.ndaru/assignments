package assignments.assignment3;

import java.util.Arrays;

class Mahasiswa extends ElemenFasilkom {

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    private long npm;
    private String tanggalLahir;
    private String jurusan;

    public Mahasiswa(String nama, long npm) {
        super("Mahasiswa", nama);
        this.npm = npm;
    }

    public long getNPM(){
        return this.npm;
    }

    public MataKuliah[] getDaftarMatkul(){
        return this.daftarMataKuliah;
    }

    public void addMatkul(MataKuliah mataKul) {
        // adds matkul, check if class already added and then checks class capacity

        if (!this.checkClassAdded(mataKul)){
            if (!this.checkClassCapacity(mataKul)){

                // finds empty slot in the array
                int indexMatkul = this.findEmptyArrayMatkul();
                this.daftarMataKuliah[indexMatkul] = mataKul;

                mataKul.addMahasiswa(this);

                System.out.println(String.format("%s berhasil menambahkan mata kuliah %s", this.getName(), mataKul.getName()));
            }
        }
    }

    public void dropMatkul(MataKuliah matkul) {
        int index = this.findMatkul(matkul);

        // index 999 is when the matkul is not found in the array
        if (index == 999) System.out.println("[DITOLAK] " + matkul.getName() + " belum pernah diambil");
        else {
            // replace the dropped matkul with null, and then shifts
            // the entire array after the dropped one to the left
            // this way it will stay organized

            this.daftarMataKuliah[index].dropMahasiswa(this);
            this.daftarMataKuliah[index] = null;

            // shifter
            for(int i = index; i < this.daftarMataKuliah.length; i++){
                if(i == this.daftarMataKuliah.length-1) {
                    this.daftarMataKuliah[i] = null;
                    break;
                }
                this.daftarMataKuliah[i] = this.daftarMataKuliah[i+1];
                if(this.daftarMataKuliah[i+1] == null) break;
            }

            System.out.println(String.format("%s berhasil drop mata kuliah %s", this.getName(), matkul.getName()));
        }
    }

    public String extractTanggalLahir(long npm) {
        if(this.tanggalLahir == null){
            String kode = Long.toString(npm);
            String date = String.format("%d-%d-%d", 
            Integer.parseInt(kode.substring(4,6)), 
            Integer.parseInt(kode.substring(6,8)), 
            Integer.parseInt(kode.substring(8,12)));

            this.tanggalLahir = date;
        }
        return this.tanggalLahir;
    }

    public MataKuliah checkMataKuliah(MataKuliah matkul){
        // checks if matakuliah already added or not
        // returns the matkul if found

        for(MataKuliah matakuliah : daftarMataKuliah){
            if(matakuliah == null) return null;
            if(matakuliah.getName().equals(matkul.getName()))
                return matkul;
        }
        return null;
    }

    public String extractJurusan(long npm) {
        if(this.jurusan == null){
            String kode = Long.toString(npm);
            switch (Integer.parseInt(kode.substring(2,4))) {
                case 01:
                    this.jurusan = "Ilmu Komputer";
                    break;
                case 02:
                    this.jurusan = "Sistem Informasi";
                    break;
                default:
                    break;
            }
        }
        return this.jurusan;
    }

    public boolean checkClassAdded(MataKuliah matkul){
        // check if class has been added to the array, true if added
        // same functionality as checkMataKuliah, the difference is this one outputs boolean
        // and also prints error message

        for(int i = 0; i < this.daftarMataKuliah.length; i++){
            if(this.daftarMataKuliah[i] == null) break;
            else if(this.daftarMataKuliah[i].getName().equals(matkul.getName())){
                System.out.println("[DITOLAK] " + matkul.getName() + " telah diambil sebelumnya");
                return true;
            }
        }
        return false;
    }

    public boolean checkClassCapacity(MataKuliah matkul){
        if (matkul.isCapacityFull()){
            System.out.println("[DITOLAK] " + matkul.getName() + " telah penuh kapasitasnya");
            return true;
        }
        return false;
    }

    public int findEmptyArrayMatkul(){
        for(int i = 0; i < this.daftarMataKuliah.length; i++){
            if (this.daftarMataKuliah[i] == null) return i;
        }
        return 999;
    }

    public boolean checkNoClassAdded(){
        // checks if there's no class added or not
        if(this.daftarMataKuliah[0] == null) return true;
        return false;
    }

    public int findMatkul(MataKuliah matkul){
        // find matkul based on the matkul name
        
        for (int i = 0; i < this.daftarMataKuliah.length; i++){
            if (this.daftarMataKuliah[i] == null) break;
            if (this.daftarMataKuliah[i].getName().equals(matkul.getName())) return i;
        }
        return 999;
    }
}