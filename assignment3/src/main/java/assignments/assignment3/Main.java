package assignments.assignment3;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];
    static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    static int totalMataKuliah = 0;
    static int totalElemenFasilkom = 0; 
    
    static ElemenFasilkom findElemen(String name){
        // finds an element within the array

        for(ElemenFasilkom elemen : daftarElemenFasilkom){
            if(elemen == null) return null;
            if(elemen.getName().equals(name)){
                return elemen;
            }
        }
        return null;
    }

    static MataKuliah findMataKuliah(String name){
        // finds an element matakuliah within the array
        
        for(MataKuliah matkul : daftarMataKuliah){
            if(matkul == null) return null;
            if(matkul.getName().equals(name)){
                return matkul;
            }
        }
        return null;
    }

    static int findEmptyArrayElemenFasilkom(String nama){
        for(int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i] == null) return i;

            // while finding, will also compare alphabetically
            else if(nama.compareTo(daftarElemenFasilkom[i].getName()) < 0){
                for(int j = daftarElemenFasilkom.length-1; j > i; j--){
                    if(j == i) break;
                    daftarElemenFasilkom[j] = daftarElemenFasilkom[j-1];
                }
                return i;
            }
        }
        return 999;
    }

    static int findEmptyArrayMataKuliah(){
        for(int i = 0; i < daftarMataKuliah.length; i++){
            if (daftarMataKuliah[i] == null) return i;
        }
        return 999;
    }

    static void addMahasiswa(String nama, long npm) {
        int index = findEmptyArrayElemenFasilkom(nama);
        daftarElemenFasilkom[index] = new Mahasiswa(nama, npm);
        totalElemenFasilkom++;
        System.out.println(nama + " berhasil ditambahkan");
    }

    static void addDosen(String nama) {
        int index = findEmptyArrayElemenFasilkom(nama);
        daftarElemenFasilkom[index] = new Dosen(nama);
        totalElemenFasilkom++;
        System.out.println(nama + " berhasil ditambahkan");
    }

    static void addElemenKantin(String nama) {
        int index = findEmptyArrayElemenFasilkom(nama);
        daftarElemenFasilkom[index] = new ElemenKantin(nama);
        totalElemenFasilkom++;
        System.out.println(nama + " berhasil ditambahkan");
    }

    static void menyapa(String objek1, String objek2) {

        // checks if object is the same
        if(objek1.equals(objek2))
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        else{
            ElemenFasilkom obj1 = findElemen(objek1);
            ElemenFasilkom obj2 = findElemen(objek2);
            obj1.menyapa(obj2);
        }
    }

    static void addMakanan(String objek, String namaMakanan, long harga) {
        ElemenFasilkom obj = findElemen(objek);

        // checks if object is ElemenKantin or not
        if(!(obj instanceof ElemenKantin))
            System.out.println(String.format("[DITOLAK] %s bukan merupakan elemen kantin", objek));
        else{
            ElemenKantin elemen = (ElemenKantin) obj;
            elemen.setMakanan(namaMakanan, harga);
        }
    }

    static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenFasilkom obj1 = findElemen(objek1);
        ElemenFasilkom obj2 = findElemen(objek2);

        // checks if object is ElemenKantin or not, and if both object is the same or not
        if(!(obj2 instanceof ElemenKantin))
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        else if(objek1.equals(objek2))
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
        else{
            obj1.membeliMakanan(obj1, obj2, namaMakanan);
        }
    }

    static void createMatkul(String nama, int kapasitas) {
        int index = findEmptyArrayMataKuliah();
        daftarMataKuliah[index] = new MataKuliah(nama, kapasitas);
        totalMataKuliah++;
        System.out.println(nama + " berhasil ditambahkan dengan kapasitas " + kapasitas);
    }

    static void addMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom obj = findElemen(objek);
        MataKuliah matkul = findMataKuliah(namaMataKuliah);

        // checks if object Mahasiswa or not
        if(!(obj instanceof Mahasiswa))
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        else{
            Mahasiswa mahasis = (Mahasiswa) obj;
            mahasis.addMatkul(matkul);
        }
    }

    static void dropMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom obj = findElemen(objek);
        MataKuliah matkul = findMataKuliah(namaMataKuliah);

        // checks if object Mahasiswa or not
        if(!(obj instanceof Mahasiswa))
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        else{
            Mahasiswa mahasis = (Mahasiswa) obj;
            mahasis.dropMatkul(matkul);
        }
    }

    static void mengajarMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom obj = findElemen(objek);
        MataKuliah matkul = findMataKuliah(namaMataKuliah);

        // checks if object Dosen or not
        if(!(obj instanceof Dosen))
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        else{
            Dosen dosen = (Dosen) obj;
            dosen.mengajarMataKuliah(matkul);
        }
    }

    static void berhentiMengajar(String objek) {
        ElemenFasilkom obj = findElemen(objek);

        // checks if object Dosen or not
        if(!(obj instanceof Dosen))
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        else{
            Dosen dosen = (Dosen) obj;
            dosen.dropMataKuliah();
        }
    }

    static void ringkasanMahasiswa(String objek) {
        ElemenFasilkom obj = findElemen(objek);

        // checks if object Mahasiswa or not
        if(!(obj instanceof Mahasiswa))
            System.out.println(String.format("[DITOLAK] %s bukan merupakan seorang mahasiswa", objek));
        else{
            Mahasiswa mahasis = (Mahasiswa) obj;
            System.out.println(String.format("Nama: %s", mahasis.getName()));
            System.out.println(String.format("Tanggal lahir: %s", mahasis.extractTanggalLahir(mahasis.getNPM())));
            System.out.println(String.format("Jurusan: %s", mahasis.extractJurusan(mahasis.getNPM())));
            System.out.println("Daftar Mata Kuliah:");
            printDaftarMatkul(mahasis);
        }
    }

    static void printDaftarMatkul(Mahasiswa mahasis){
        // prints the list of matkul the Mahasiswa takes

        if(mahasis.checkNoClassAdded())
            System.out.println("Belum ada mata kuliah yang diambil");
        else{
            MataKuliah[] daftar = mahasis.getDaftarMatkul();
            for(int i = 0; i < daftar.length; i++){
                if(daftar[i] == null) break;
                System.out.println(String.format("%d. %s", (i+1), daftar[i].getName()));
            }
        }
    }

    static void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah matkul = findMataKuliah(namaMataKuliah);
        System.out.println(String.format("Nama mata kuliah: %s", matkul.getName()));
        System.out.println(String.format("Jumlah mahasiswa: %d", matkul.getKapasitasSekarang()));
        System.out.println(String.format("Kapasitas: %d", matkul.getKapasitas()));

        if(matkul.getDosen() == null)
            System.out.println("Dosen pengajar: Belum ada");
        else
            System.out.println(String.format("Dosen pengajar: %s", matkul.getDosen()));
        
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");
        printDaftarMahasiswa(matkul);
    }

    static void printDaftarMahasiswa(MataKuliah matkul){
        // prints the list of Mahasiswa taking the matkul

        if(matkul.getKapasitasSekarang() == 0)
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        else{
            Mahasiswa[] daftar = matkul.getDaftarMahasiswa();
            for(int i = 0; i < daftar.length; i++){
                if(daftar[i] == null) break;
                System.out.println(String.format("%d. %s", (i+1), daftar[i].getName()));
            }
        }
    }

    static void nextDay() {
        for(ElemenFasilkom elemen : daftarElemenFasilkom){
            if(elemen == null) break;
            else{
                // if the current greeting amount is greater or equal to 50% of the total element excluding itself
                // will add friendship by 10
                // else will decrease by 5
                
                if(elemen.getCurrentGreeting() >= (0.5*(totalElemenFasilkom-1)))
                    elemen.setFriendship(elemen.getFriendship() + 10);
                else if(elemen.getCurrentGreeting() < (0.5*(totalElemenFasilkom-1)))
                    elemen.setFriendship(elemen.getFriendship() - 5);
                elemen.resetMenyapa();
            }
        }
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
    }

    static void friendshipRanking() {
        int count = 1;
        ElemenFasilkom[] ranked = rankedListGenerator();
        for(ElemenFasilkom elemen : ranked){
            if(elemen == null) break;
            System.out.println(String.format("%d. %s(%d)", count, elemen.getName(), elemen.getFriendship()));
            count++;
        }
    }

    static ElemenFasilkom[] rankedListGenerator(){
        // creates a new Array that has its content sorted
        // using selection sort, reference obtained from
        // https://www.geeksforgeeks.org/selection-sort/

        ElemenFasilkom[] ranked = basedByScore(daftarElemenFasilkom);
        ElemenFasilkom[] rankedSorted = basedByName(ranked);
        return rankedSorted;
    }

    static ElemenFasilkom[] basedByScore(ElemenFasilkom[] rankedInput){

        // sort based from friendship score

        // creates a new array
        ElemenFasilkom[] ranked = new ElemenFasilkom[100];
        for(int i = 0; i < rankedInput.length; i++){
            if(rankedInput[i] == null) break;
            ranked[i] = rankedInput[i];
        }

        // does the selection sort
        for(int i = 0; i < ranked.length-1; i++){
            int max = i;
            for(int j = i+1; j < ranked.length; j++){
                if(ranked[j] == null) break;
                if(ranked[j].getFriendship() > ranked[i].getFriendship() && ranked[j].getFriendship() > ranked[max].getFriendship()){
                    max = j;
                }
            }
            if(i == max) continue;

            ElemenFasilkom temp = ranked[max];
            ranked[max] = ranked[i];
            ranked[i] = temp;
            if(ranked[i] == null) break;
        }
        return ranked;
    }

    static ElemenFasilkom[] basedByName(ElemenFasilkom[] rankedInput){

        // sort based from name

        // creates a new array
        ElemenFasilkom[] ranked = new ElemenFasilkom[100];
        for(int i = 0; i < rankedInput.length; i++){
            if(rankedInput[i] == null) break;
            ranked[i] = rankedInput[i];
        }

        // does the selection sort
        for(int i = 0; i < ranked.length-1; i++){
            int max = i;
            for(int j = i+1; j < ranked.length; j++){
                if(ranked[j] == null) break;
                if((ranked[j].getFriendship() == ranked[i].getFriendship()) && 
                (ranked[j].getName().compareTo(ranked[i].getName()) < 0) &&
                (ranked[j].getName().compareTo(ranked[max].getName()) < 0))
                    max = j;
            }
            if(i == max) continue;

            ElemenFasilkom temp = ranked[max];
            ranked[max] = ranked[i];
            ranked[i] = temp;
            if(ranked[i] == null) break;
        }
        return ranked;
    }

    static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    input.close();
    }
}