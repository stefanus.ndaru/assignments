package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {

    private Makanan[] daftarMakanan = new Makanan[10];

    public ElemenKantin(String nama) {
        super("Elemen Kantin", nama);
    }

    public boolean checkMakanan(String nama){
        // checks if food already added

        for(Makanan food : daftarMakanan){
            if(food == null) return false;
            if(food.getName().equals(nama)) return true;
        }
        return false;
    }

    public int findEmptyArrayFood(){
        for(int i = 0; i < this.daftarMakanan.length; i++){
            if (this.daftarMakanan[i] == null) return i;
        }
        return 999;
    }

    public void setMakanan(String nama, long harga) {
        // sets this elements list of food
        // prints error if food is already added

        if(this.checkMakanan(nama)){
            System.out.println(String.format("[DITOLAK] %s sudah pernah terdaftar", nama));
        }
        else{
            int index = this.findEmptyArrayFood();
            daftarMakanan[index] = new Makanan(nama, harga);
            System.out.println(String.format("%s telah mendaftarkan makanan %s dengan harga %d", this.getName(), nama, harga));
        }
    }

    public Makanan findSpecificFood(String foodName){
        // finds a specific food based off the name
        // returns it when found
        
        for(Makanan food : daftarMakanan){
            if(food == null) return null;
            if(food.getName().equals(foodName))
                return food;
        }
        return null;
    }
}